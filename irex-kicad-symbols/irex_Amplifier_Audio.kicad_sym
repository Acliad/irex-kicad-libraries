(kicad_symbol_lib
	(version 20231120)
	(generator "kicad_symbol_editor")
	(generator_version "8.0")
	(symbol "MAX98357xT"
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(property "Reference" "U"
			(at -13.97 16.51 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MAX98357xT"
			(at -13.97 13.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Package_DFN_QFN:TQFN-16-1EP_3x3mm_P0.5mm_EP1.23x1.23mm"
			(at 0 -31.242 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "https://www.analog.com/media/en/technical-documentation/data-sheets/MAX98357A-MAX98357B.pdf"
			(at 0 -28.194 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" "Mono DAC with amplifier, I2S, PCM, TDM, 32-bit, 96khz, 3.2W, TQFN-16"
			(at 0 -25.146 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "ki_keywords" "audio amplifier class d class-d i2s pcm"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "ki_fp_filters" "*TQFN*16*3x3*EP1.2"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(symbol "MAX98357xT_0_1"
			(rectangle
				(start -12.7 12.7)
				(end 12.7 -10.16)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type background)
				)
			)
		)
		(symbol "MAX98357xT_1_1"
			(pin input line
				(at -15.24 0 0)
				(length 2.54)
				(name "DIN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin output line
				(at 15.24 0 180)
				(length 2.54)
				(name "OUTN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "10"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 -12.7 90)
				(length 2.54) hide
				(name "GND"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "11"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 8.89 15.24 270)
				(length 2.54) hide
				(name "NC"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "12"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 6.35 -12.7 90)
				(length 2.54) hide
				(name "NC"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "13"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 2.54 0)
				(length 2.54)
				(name "LRCLK"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "14"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 -12.7 90)
				(length 2.54) hide
				(name "GND"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "15"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 5.08 0)
				(length 2.54)
				(name "BCLK"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "16"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin free line
				(at 2.54 -12.7 90)
				(length 2.54)
				(name "EP"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "17"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 -5.08 0)
				(length 2.54)
				(name "GAIN_SLOT"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 -12.7 90)
				(length 2.54)
				(name "GND"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "3"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 7.62 0)
				(length 2.54)
				(name "~{SD_MODE}"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "4"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 8.89 -12.7 90)
				(length 2.54) hide
				(name "NC"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "5"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 6.35 15.24 270)
				(length 2.54) hide
				(name "NC"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "6"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 15.24 270)
				(length 2.54)
				(name "VDD"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "7"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 15.24 270)
				(length 2.54) hide
				(name "VDD"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "8"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin output line
				(at 15.24 5.08 180)
				(length 2.54)
				(name "OUTP"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "9"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
		)
	)
	(symbol "MAX98357xW"
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(property "Reference" "U"
			(at 1.27 16.51 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Value" "MAX98357xW"
			(at 1.27 13.97 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left)
			)
		)
		(property "Footprint" "Package_BGA:WLP-9_1.448x1.468mm_Layout3x3_P0.4mm_Ball0.27mm_Pad0.25mm"
			(at 0.254 -30.226 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Datasheet" "https://www.analog.com/media/en/technical-documentation/data-sheets/MAX98357A-MAX98357B.pdf"
			(at 0 -27.178 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Description" "Mono DAC with amplifier, I2S, PCM, TDM, 32-bit, 96khz, 3.2W, WLP-9"
			(at 0 -24.13 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "ki_keywords" "audio amplifier class d class-d i2s pcm"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "ki_fp_filters" "*WLP-9*P0.4"
			(at 0 0 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(symbol "MAX98357xW_0_1"
			(rectangle
				(start -12.7 12.7)
				(end 12.7 -10.16)
				(stroke
					(width 0)
					(type default)
				)
				(fill
					(type background)
				)
			)
		)
		(symbol "MAX98357xW_1_1"
			(pin input line
				(at -15.24 7.62 0)
				(length 2.54)
				(name "~{SD_MODE}"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "A1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 15.24 270)
				(length 2.54)
				(name "VDD"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "A2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin output line
				(at 15.24 5.08 180)
				(length 2.54)
				(name "OUTP"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "A3"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 0 0)
				(length 2.54)
				(name "DIN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "B1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 -5.08 0)
				(length 2.54)
				(name "GAIN_SLOT"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "B2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin output line
				(at 15.24 0 180)
				(length 2.54)
				(name "OUTN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "B3"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 5.08 0)
				(length 2.54)
				(name "BCLK"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "C1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 -12.7 90)
				(length 2.54)
				(name "GND"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "C2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -15.24 2.54 0)
				(length 2.54)
				(name "LRCLK"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "C3"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
		)
	)
)
